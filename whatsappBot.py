from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver import ActionChains
from time import sleep
import os

class WhatappBot:
    def __init__(self):
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
        self.driver.get("https://web.whatsapp.com")

        try:
            while self.driver.find_element_by_xpath("//div[text()='To use WhatsApp on your computer:']"):
                sleep(2)
        except:
            sleep(2)

    def createGroupChat(self, members, group_name, icon_path):
        """
        create a group chat called 'group_name' with the members in 'members'
        :param members: the users that will be added to the group chat
        :param group_name: the group name
        :param icon_path: the group's icon path
        :type: strings
        :return: n/a
        """
        self.driver.find_element_by_xpath("//div[@title='Menu']").click()
        sleep(1)
        self.driver.find_element_by_xpath("//div[@aria-label='New group']").click()
        sleep(1)

        #adds the users to group chat
        for name in members:
            #enter name to input filed field
            self.driver.find_element_by_xpath("//input[@class='_1x9wV copyable-text selectable-text']").send_keys(name)
            sleep(1)
            #click on the user name to add to group
            self.driver.find_element_by_xpath("//span[@title='{}']".format(name)).click()
            sleep(1)

        #press to continue
        self.driver.find_element_by_xpath("//span[@data-icon='arrow-forward']").click()
        sleep(1)
        
        #enter group name
        self.driver.find_element_by_xpath("//div[@class='_13NKt copyable-text selectable-text']").send_keys(group_name)
        sleep(1)
        
        #add group icon
        sleep(1)
        self.driver.find_element_by_xpath("//input[@type='file']").send_keys(icon_path)
        sleep(1)
        self.driver.find_element_by_xpath("//span[@data-icon='checkmark-large']").click()
        sleep(3)

        #press to continue
        self.driver.find_element_by_xpath("//span[@data-icon='checkmark-medium']").click()
        sleep(1)

    def exitGroupChat(self, group_name):
        """
        Exit the group chat that was made.
        :param group name: the name of the group
        :type: string
        :return: groupLink
        """
        #the link to the created group
        groupLink = self.driver.find_element_by_xpath("//span[@title='{}']".format(group_name))
        sleep(1)
        #right click on it
        ActionChains(self.driver).context_click(groupLink).perform()
        sleep(2)
        #press 'Exit group'
        self.driver.find_element_by_xpath("//div[text()='Exit group']").click()
        sleep(2)
        #press 'Exit'
        self.driver.find_element_by_xpath("//div[text()='Exit group']").click()
        #return the group link
        return groupLink


    def deleteGroup(self, groupLink):
        """
        deletes the created group.
        :param groupLink: the link to group chat
        :type: 'webElement'
        :return: n/a
        """
        #right click on group chat
        ActionChains(self.driver).context_click(groupLink).perform()
        sleep(1)
        #press 'delete group'
        self.driver.find_element_by_xpath("//div[text()='Delete group']").click()
        sleep(1)

        #press 'Delete'    
        self.driver.find_element_by_xpath("//div[text()='Delete group']").click()


    def refresh(self):
        self.driver.refresh()
        sleep(10)


def get_file_full_path(filename):
    here = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(here, filename)

def main():

    bot = WhatappBot()

    num_of_groups = 30
    participants = ["Doctor", "Yahelson"]
    

    for i in range(1, num_of_groups + 1):
        
        group_name = "Doc the hacker {}".format(i)
        bot.createGroupChat(participants, group_name, get_file_full_path("group_icon.jpg"))
        sleep(5)
        bot.refresh()
        groupLink = bot.exitGroupChat(group_name)
        sleep(3)
        bot.deleteGroup(groupLink)
        sleep(1)


if __name__ == "__main__":
    main()